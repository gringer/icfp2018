# icfp2018

Source code for 2018 ICFP Contest - David Eccles (gringer) <bioinformatics@gringene.org>

This is an R implementation of the Nanobot Matter Manipulation System to test out the current capabilities of R. 
It uses the magrittr pipe operator "%>%" to simplify scripting and preserve state during command operations, 
and writes a trace of operations to a file as a side effect of command execution.

## Running the Code

Library packages to load and accessory methods appear at the top of the source file. Solutions for particular
problems appear at the bottom of the source file. These solutions have been manually crafted. I appreciate
that the task designers have provided trace execution services, which has made it a lot easier for me to
debug my solutions.

A solution follows a fairly simple process; see code examples at the end of the script:

1. Load the initial state of the system using 'systemState %<>% loadBoard(<problem_location.mdl>) %>%'. This
   truncates the file 'trace.nbt' in the working directory.
2. Carry out operations as per the task for the problem using <op> %>% [a trivial task, left as an exercise for the reader].
3. Carry out the Halt(1) operation. This closes the file 'trace.nbt'. If halt doesn't result in a correct solution,
   an image of the Y-plane including bot #0 is created and displayed.
4. Manually copy the solution [using the file system] to the solution location

## Notes

* As of 2018-07-23 09:57:52 UTC, my solutions appear to be in first place in all the puzzles that I have attempted. 
  If this continues (and past experience suggests it won't), I think that this is reasonable grounds for me to
  self-nominate for the judges prize, particularly considering how unusual it is for R to be used in the ICFP contest.
  I acknowledge that I've just been doing manual tinkering, but that fits the exploration carried out in some previous
  competitions.

* I have tried to make scripts that are [what I believe to be] as optimal as possible. The FA019 and FA020 scripts
  may have additional optimisations possible, as they have two correction moves of one block. FA011 also has at least
  one cycle that could be eliminated by more careful bot positioning.

* "Note that some high-bits of the last byte of the model file may be unused." -- why??? (cube numbers and byte counts)

* Copying a nanobot to a new location, then removing the old one is energy-neutral
  - *but* each nanobot requires 20 energy per step to maintain
   - all commands execute simultaneously at the end of the step, so needs at least one
     turn with 2x nanobots for movement; i.e. fusion/fission is not a good way to move
  - so move as far as possible with one bot, then fiss
  - carry out move/wait, then fuse, then move

* A single cycle uses a *lot* of energy; the best optimisations should prioritise reducing cycles
  - one typical example for a field no larger than 30x30x30: sending a bot to an orthogonal corner
    that already has a bot in it, and sending that other corner bot to the diagonal corner
    (2 cycles) vs fission from orthogonal corner (3 cycles)

* Fill + Void is energy-neutral

* Clearing an already-empty space only uses 3 energy; i.e. one more than it takes to move in
  a straight line *through* that space

## Major Challenges

* Making sure that operations involving more than one bot were executed only once per cycle

* Making sure that operations within cycles didn't stop other same-cycle operations from working
