source("botFunctions.r");

## Generic disassembly solver (where resolution <= 30)
## 1. Clone to 4/8 nanobots
## 2. Move to edges of bounding box
## 3. Activate GVoid
## 4. Return to origin

## FD003 [planar model]

systemState %<>% 
  loadBoard("../problems/FD003_src.mdl") %>%
  Fission(1, c(0,0,1), 4);
xRange <- range(which(apply(systemState$target,1,sum) != 0))-1; ## X
yRange <- range(which(apply(systemState$target,2,sum) != 0))-1; ## Y
zRange <- range(which(apply(systemState$target,3,sum) != 0))-1; ## Z

while(systemState$bots[[2]]$pos[3] < max(zRange)){
  zDiff <- min(15, max(zRange+1) - systemState$bots[[2]]$pos[3]);
  systemState %<>%
    Wait(1) %>%
    SMove(2, c(0,0,zDiff));
}
systemState <- Fission(systemState, 1, c(1,0,0), 4) %>%
  Fission(2, c(1,0,0), 2);
while(systemState$bots[[3]]$pos[1] < max(xRange)){
  xDiff <- min(15, max(xRange+1) - systemState$bots[[3]]$pos[1]);
  systemState %<>% 
    Wait(1) %>%
    Wait(2) %>%
    SMove(3, c(xDiff,0,0)) %>%
    SMove(7, c(xDiff,0,0));
}
systemState %<>% 
  GVoid(1, c(1,0,1), c(diff(xRange),0,diff(zRange))) %>%
  GVoid(2, c(1,0,-1), c(diff(xRange),0,-diff(zRange))) %>%
  GVoid(3, c(-1,0,-1), c(-diff(xRange),0,-diff(zRange))) %>%
  GVoid(7, c(-1,0,1), c(-diff(xRange),0,diff(zRange)))
while(systemState$bots[[3]]$pos[1] > 1){
  xDiff <- min(15, systemState$bots[[3]]$pos[1] - 1);
  systemState %<>% 
    Wait(1) %>%
    Wait(2) %>%
    SMove(3, c(-xDiff,0,0)) %>%
    SMove(7, c(-xDiff,0,0));
}
systemState %<>% 
  FusionP(1, c(1,0,0) ,7, c(-1,0,0)) %>%
  FusionP(2, c(1,0,0), 3, c(-1,0,0)) %>%
  FusionS(3, c(-1,0,0), 2, c(1,0,0)) %>%
  FusionS(7, c(-1,0,0), 1, c(1,0,0));
while(systemState$bots[[2]]$pos[3] > 1){
  zDiff <- min(15, systemState$bots[[2]]$pos[3] - 1);
  systemState %<>% 
    Wait(1) %>%
    SMove(2, c(0,0,-zDiff))
}
systemState %<>% 
  FusionP(1, c(0,0,1), 2, c(0,0,-1)) %>%
  FusionS(2, c(0,0,-1), 1, c(0,0,1)) %>%
  Halt(1);
