#!/usr/bin/Rscript

## A model Mdl specifies a 3D object. A model is comprised of the resolution R 
## of a matrix (which is large enough to contain the object) and the set of Full
## coordinates of the matrix that make up the object.

## A model is well-formed if

## 1. All coordinates of the set of Full coordinates must not belong to the
##    reserved left-, right-, top-, near-, or far-face regions of the space.
##    That is, all Full coordinates (x, y, z) satisfy 1 ≤ x ≤ R - 2 and 
##    0 ≤ y ≤ R - 2 and 1 ≤ z ≤ R - 2.
## 2. All coordinates of the set of Full coordinates must be grounded. That
##    is, all Full coordinates c = (x, y, z) satisfy either y = 0 or there
##    exists an adjacent Full coordinate c′ = c + d (where mlen(d) = 1) that
##    is grounded.

## *** Model Files ***

## A model file is binary encoding of a model. (Note that this binary encoding 
## handles both ill-formed and well-formed models.)

## By convention, a model file has the extension .mdl.

## The first byte of the model file encodes the resolution R, interpreting the
## byte as an 8-bit (unsigned) integer.

## The remaining ⌈(R × R × R) / 8⌉ bytes of the model file encode the set of
## Full coordinates. The sequence of bytes are interpreted as a sequence of
## bits corresponding to the coordinates of the matrix by traversing the
## x-dimension from 0 to R - 1, traversing the y-dimension from 0 to R - 1,
## and traversing the z-dimension from 0 to R - 1. More explicitly, 
## coordinate (x, y, z) is Full in the model’s matrix if and only if
## bit x × R × R + y × R + z is set. Note that some high-bits of the last
## byte of the model file may be unused.

library(rgl);
library(bit);
library(bitops);

modelFile <- "problems/LA042_tgt.mdl";
fileLen <- file.size(modelFile);
fileData <- readBin(modelFile, what=integer(), size=1, signed=FALSE,
                    n=fileLen);
chunkMat <- 2^(0:7);
modelRes <- fileData[1];
fileData <- fileData[-1];
matrixData <- as.bit(bitAnd(rep(fileData, each=8), chunkMat) > 0);
matrixData <- head(matrixData, modelRes^3);
dim(matrixData) <- rep(modelRes, 3);


if(modelRes < 45){
  clear3d();
  plot3d(NA, xlab="X", ylab="Z", zlab="Y",
         xlim=c(0,modelRes), ylim=c(0,modelRes), zlim=c(0,modelRes));
  shapelist3d(
    xlim=c(0,modelRes), ylim=c(0,modelRes), zlim=c(modelRes, 0),
    cube3d(trans=scaleMatrix(0.45,0.45,0.45)),
    x=(rep(1:modelRes, each=modelRes^2))[matrixData], 
    z=(rep(1:modelRes, each=modelRes, times=modelRes))[matrixData], 
    y=(rep(1:modelRes, times=modelRes^2))[matrixData],
    col="grey");
} else {
  clear3d();
  plot3d(xlab="X", ylab="Z", zlab="Y",
         xlim=c(0,modelRes), ylim=c(0,modelRes), zlim=c(0,modelRes),
         x=(rep(1:modelRes, each=modelRes^2))[matrixData], 
         z=(rep(1:modelRes, each=modelRes, times=modelRes))[matrixData], 
         y=(rep(1:modelRes, times=modelRes^2))[matrixData],
         col = (matrixData+0)[matrixData]);
}

