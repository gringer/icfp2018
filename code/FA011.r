source("botFunctions.r");

xRange <- range(which(apply(systemState$target,1,sum) != 0))-1; ## X
yRange <- range(which(apply(systemState$target,2,sum) != 0))-1; ## Y
zRange <- range(which(apply(systemState$target,3,sum) != 0))-1; ## Z


## 'I' character (FA011) (uses 8 nanobots)
systemState %<>% loadBoard("../problems/FA011_tgt.mdl") %>%
  LMove(1, c(5,0,0), c(0,0,5)) %>%
  Fission(1, c(1,0,0), 5) %>%
  Fission(1, c(0,0,1), 5) %>%
  SMove(2, c(6,0,0)) %>%
  Fission(1, c(1,0,0), 5) %>%
  SMove(2, c(0,0,7)) %>%
  SMove(8, c(0,0,6)) %>%
  LMove(1, c(0,0,1), c(1,0,0)) %>%
  Fission(2, c(0,1,0), 2) %>%
  Fission(8, c(1,1,0), 2) %>%
  SMove(14, c(6,0,0)) %>%
  SMove(1, c(0,15,0)) %>%
  Fission(2, c(0,0,-1), 1) %>%
  SMove(3, c(0,15,0)) %>%
  Fission(8, c(1,0,-1), 1) %>%
  SMove(9, c(0,15,0)) %>%
  LMove(14, c(0,0,1), c(0,5,0)) %>%
  SMove(1, c(0,3,0)) %>%
  Wait(2) %>%
  SMove(3, c(0,2,0)) %>%
  SMove(6, c(0,0,-5)) %>%
  SMove(8, c(1,0,0)) %>%
  SMove(9, c(0,2,0)) %>%
  SMove(12, c(0,0,-5)) %>%
  SMove(14, c(0,13,0)) %>%
  ## Whoops, it's not solid! [Wasted cycle]
  SMove( 1, c( 2,0,0)) %>%
  SMove( 2, c(-2,0,0)) %>%
  SMove( 3, c(-2,0,0)) %>%
  SMove( 6, c(-2,0,0)) %>%
  SMove( 8, c( 2,0,0)) %>%
  SMove( 9, c( 2,0,0)) %>%
  SMove(12, c( 2,0,0)) %>%
  SMove(14, c(-2,0,0)) %>%
  ## Left and Right
  GFill(1,  c(-1,0, 1), c( 0,-18, 4)) %>%
  GFill(2,  c( 1,0,-1), c( 0, 18,-4)) %>%
  GFill(3,  c( 1,0,-1), c( 0,-18,-4)) %>%
  GFill(6,  c( 1,0, 1), c( 0, 18, 4)) %>%
  GFill(8,  c(-1,0,-1), c( 0, 18,-4)) %>%
  GFill(9,  c(-1,0,-1), c( 0,-18,-4)) %>%
  GFill(12, c(-1,0, 1), c( 0, 18, 4)) %>%
  GFill(14, c( 1,0, 1), c( 0,-18, 4)) %>%
  ## Top and Bottom
  GFill(1,  c(0,0, 1), c( 2,0, 4)) %>%
  GFill(2,  c(0,0,-1), c(-2,0,-4)) %>%
  GFill(3,  c(0,0,-1), c(-2,0,-4)) %>%
  GFill(6,  c(0,0, 1), c(-2,0, 4)) %>%
  GFill(8,  c(0,0,-1), c( 2,0,-4)) %>%
  GFill(9,  c(0,0,-1), c( 2,0,-4)) %>%
  GFill(12, c(0,0, 1), c( 2,0, 4)) %>%
  GFill(14, c(0,0, 1), c(-2,0, 4)) %>%
## Back and Front
  GFill(1,  c(0,-1, 1), c( 2,-16, 0)) %>%
  GFill(2,  c(0, 1,-1), c(-2, 16, 0)) %>%
  GFill(3,  c(0,-1,-1), c(-2,-16, 0)) %>%
  GFill(6,  c(0, 1, 1), c(-2, 16, 0)) %>%
  GFill(8,  c(0, 1,-1), c( 2, 16, 0)) %>%
  GFill(9,  c(0,-1,-1), c( 2,-16, 0)) %>%
  GFill(12, c(0, 1, 1), c( 2, 16, 0)) %>%
  GFill(14, c(0,-1, 1), c(-2,-16, 0)) %>%
  LMove( 1, c(-4,0,0), c(0,-5,0)) %>%
  LMove( 2, c(0,1,0), c(-5,0,0)) %>%
  LMove( 3, c(0,-5,0), c(-5,0,0)) %>%
  LMove( 6, c(0,1,0), c(-5,0,0)) %>%
  SMove( 8, c(-3,0,0)) %>%
  LMove( 9, c(-4,0,0), c(0,-5,0)) %>%
  SMove(12, c(-3,0,0)) %>%
  LMove(14, c(0,-5,0), c(-5,0,0)) %>%
  FusionP( 1, c(1,0,0),14,c(-1,0,0)) %>%
  FusionS( 2,c(0,-1,0), 8, c(0,1,0)) %>%
  FusionS( 3, c(-1,0,0),9, c(1,0,0)) %>%
  FusionS( 6,c(0,-1,0),12, c(0,1,0)) %>%
  FusionP( 8, c(0,1,0), 2,c(0,-1,0)) %>%
  FusionP( 9, c(1,0,0), 3,c(-1,0,0)) %>%
  FusionP(12, c(0,1,0), 6,c(0,-1,0)) %>%
  FusionS(14,c(-1,0,0), 1, c(1,0,0)) %>%
  SMove(1, c(-4,0,0)) %>%
  LMove( 8, c(0,0,-5), c(-4,0,0)) %>%
  LMove( 9, c(0,0,-5), c(-4,0,0)) %>%
  SMove(12, c(-4,0,0)) %>%
  FusionP( 1, c(0,0,1), 9,c(0,0,-1)) %>%
  FusionS( 8,c(0,0,-1),12, c(0,0,1)) %>%
  FusionS( 9,c(0,0,-1), 1, c(0,0,1)) %>%
  FusionP(12, c(0,0,1), 8,c(0,0,-1)) %>%
  SMove(1, c(0,-13,0)) %>%
  Wait(12) %>%
  FusionP( 1,c(1,0,0),12,c(-1,0,0)) %>%
  FusionS(12,c(-1,0,0), 1,c(1,0,0)) %>%
  SMove(1, c(0,0,-6)) %>%
  Halt(1);

