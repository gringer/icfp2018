source("botFunctions.r");

## Fill a hollow cube (FA020)

systemState <- loadBoard(systemState, "../problems/FA020_tgt.mdl") %>%
  SMove(1, c(1,0,0)) %>%
  Fission(1, c(0,1,0), 15) %>%
  Fission(1, c(1,0,0), 10) %>%
  SMove(2, c(0,15,0)) %>%
  Fission(1, c(1,0,1), 7) %>%
  SMove(2, c(0,11,0)) %>%
  SMove(18, c(15,0,0)) %>%
  Wait(1) %>%
  Fission(2, c(0,0,1), 10) %>%
  SMove(18, c(10,0,0)) %>%
  SMove(29, c(0,0,15)) %>%
  Wait(1) %>%
  Fission(2, c(1,0,0), 2) %>%
  SMove(3, c(0,0,15)) %>%
  Fission(18, c(0,0,1), 5) %>%
  SMove(29, c(0,0,13)) %>%
  Wait(1) %>%
  Wait(2) %>%
  SMove(3, c(0,0,13)) %>%
  SMove(14, c(15,0,0)) %>%
  Wait(18) %>%
  SMove(19, c(0,0,15)) %>%
  Fission(29, c(0,1,0), 5) %>%
  Wait(1) %>%
  Wait(2) %>%
  SMove(3, c(15,0,0)) %>%
  SMove(14, c(10,0,0)) %>%
  Wait(18) %>%
  SMove(19, c(0,0,13)) %>%
  Wait(29) %>%
  SMove(30, c(0,15,0)) %>%
  SMove(1, c(1,0,0)) %>%
  SMove(2, c(1,0,0)) %>%
  SMove(3, c(11,0,0)) %>%
  Wait(14) %>%
  Wait(18) %>%
  Wait(19) %>%
  Wait(29) %>%
  SMove(30, c(0,11,0)) %>%
  ## 28x28x28 cube, with bots positioned on the plane of the cube at front and rear edges
  ## left/right walls first
  GFill(1,  c(-1, 0, 1), c(  0, 27, 27)) %>% ## left  bottom front
  GFill(2,  c(-1, 0, 1), c(  0,-27, 27)) %>% ## left  top    front
  GFill(3,  c( 1, 0,-1), c(  0,-27,-27)) %>% ## right top    rear
  GFill(14, c( 1, 0, 1), c(  0,-27, 27)) %>% ## right top    front
  GFill(18, c( 1, 0, 1), c(  0, 27, 27)) %>% ## right bottom front
  GFill(19, c( 1, 0,-1), c(  0, 27,-27)) %>% ## right bottom rear
  GFill(29, c(-1, 0,-1), c(  0, 27,-27)) %>% ## left  bottom rear
  GFill(30, c(-1, 0,-1), c(  0,-27,-27)) %>% ## left  top    rear
  ## top/bottom planes
  GFill(1,  c( 0, 0, 1), c( 25,  0, 27)) %>% ## left  bottom front
  GFill(2,  c( 0, 0, 1), c( 25,  0, 27)) %>% ## left  top    front
  GFill(3,  c( 0, 0,-1), c(-25,  0,-27)) %>% ## right top    rear
  GFill(14, c( 0, 0, 1), c(-25,  0, 27)) %>% ## right top    front
  GFill(18, c( 0, 0, 1), c(-25,  0, 27)) %>% ## right bottom front
  GFill(19, c( 0, 0,-1), c(-25,  0,-27)) %>% ## right bottom rear
  GFill(29, c( 0, 0,-1), c( 25,  0,-27)) %>% ## left  bottom rear
  GFill(30, c( 0, 0,-1), c( 25,  0,-27)) %>% ## left  top    rear
  ## front/back walls last
  GFill(1,  c( 0, 1, 1), c( 25, 25,  0)) %>% ## left  bottom front
  GFill(2,  c( 0,-1, 1), c( 25,-25,  0)) %>% ## left  top    front
  GFill(3,  c( 0,-1,-1), c(-25,-25,  0)) %>% ## right top    rear
  GFill(14, c( 0,-1, 1), c(-25,-25,  0)) %>% ## right top    front
  GFill(18, c( 0, 1, 1), c(-25, 25,  0)) %>% ## right bottom front
  GFill(19, c( 0, 1,-1), c(-25, 25,  0)) %>% ## right bottom rear
  GFill(29, c( 0, 1,-1), c( 25, 25,  0)) %>% ## left  bottom rear
  GFill(30, c( 0,-1,-1), c( 25,-25,  0)) %>% ## left  top    rear
  SMove(1, c(-2,0,0)) %>%
  SMove(2, c(-2,0,0)) %>%
  SMove(3, c(-15,0,0)) %>%
  SMove(14, c(-15,0,0)) %>%
  SMove(18, c(-15,0,0)) %>%
  SMove(19, c(-15,0,0)) %>%
  SMove(29, c(-2,0,0)) %>%
  SMove(30, c(-2,0,0)) %>%
  Wait(1) %>%
  Wait(2) %>%
  SMove(3, c(-11,0,0)) %>%
  SMove(14, c(-11,0,0)) %>%
  SMove(18, c(-11,0,0)) %>%
  SMove(19, c(-11,0,0)) %>%
  Wait(29) %>%
  Wait(30) %>%
  FusionP( 1, c( 1,0,0), 18, c(-1,0,0)) %>%
  FusionP( 2, c( 1,0,0), 14, c(-1,0,0)) %>%
  FusionS( 3, c(-1,0,0), 30, c( 1,0,0)) %>%
  FusionS(14, c(-1,0,0),  2, c( 1,0,0)) %>%
  FusionS(18, c(-1,0,0),  1, c( 1,0,0)) %>%
  FusionS(19, c(-1,0,0), 29, c( 1,0,0)) %>%
  FusionP(29, c( 1,0,0), 19, c(-1,0,0)) %>%
  FusionP(30, c( 1,0,0),  3, c(-1,0,0)) %>%
  Wait(1) %>%
  SMove(2, c(0,-15,0)) %>%
  Wait(29) %>%
  SMove(30, c(0,-15,0)) %>%
  Wait(1) %>%
  SMove(2, c(0,-11,0)) %>%
  Wait(29) %>%
  SMove(30, c(0,-11,0)) %>%
  FusionP(1, c(0,1,0), 2, c(0,-1,0)) %>%
  FusionS(2, c(0,-1,0), 1, c(0,1,0)) %>%
  FusionP(29, c(0,1,0), 30, c(0,-1,0)) %>%
  FusionS(30, c(0,-1,0), 29, c(0,1,0)) %>%
  Wait(1) %>%
  SMove(29, c(0,0,-15)) %>%
  Wait(1) %>%
  SMove(29, c(0,0,-13)) %>%
  FusionP(1, c(0,0,1), 29, c(0,0,-1)) %>%
  FusionS(29, c(0,0,-1), 1, c(0,0,1)) %>%
  Halt(1);
