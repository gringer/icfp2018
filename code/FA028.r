source("botFunctions.r");

## Hollow Pyramid (FA028) (uses 4 nanobots)
## * note: walls are two blocks thick, which means fill+void can't be used at the same time

systemState %<>% loadBoard("../problems/FA028_tgt.mdl") %>%
  Fission(1, c(0,0,1), 5) %>%
  Fission(1, c(1,0,0), 5) %>%
  SMove(2, c(0,0,15)) %>%
  Fission(1, c(0,0,1), 5) %>%
  SMove(2, c(0,0,13)) %>%
  SMove(8, c(15,0,0)) %>%
  Wait(1) %>%
  SMove(2, c(15,0,0)) %>%
  SMove(8, c(13,0,0)) %>%
  SMove(14, c(0,0,15)) %>%
  Wait(1) %>%
  SMove(2, c(14,0,0)) %>%
  Wait(8) %>%
  SMove(14, c(0,0,13)) %>%
  GFill(1, c(1,0,1), c(27,0,27)) %>%
  GFill(2, c(-1,0,-1), c(-27,0,-27)) %>%
  GFill(8, c(-1,0,1), c(-27,0,27)) %>%
  GFill(14, c(1,0,-1), c(27,0,-27)) %>%
  LMove(1, c(3,0,0), c(0,1,0)) %>%  
  LMove(2, c(-3,0,0), c(0,1,0)) %>%  
  LMove(8, c(-3,0,0), c(0,1,0)) %>%  
  LMove(14, c(3,0,0), c(0,1,0)) %>%  
  SMove(1, c(0,0,3)) %>%  
  SMove(2, c(0,0,-3)) %>%  
  SMove(8, c(0,0,3)) %>%  
  SMove(14, c(0,0,-3)) %>%  
  GVoid(1, c(0,-1,0), c(23,0,23)) %>%
  GVoid(2, c(0,-1,0), c(-23,0,-23)) %>%
  GVoid(8, c(0,-1,0), c(-23,0,23)) %>%
  GVoid(14, c(0,-1,0), c(23,0,-23)) %>%
  LMove( 1, c(-2,0,0), c(0,0,-2)) %>%  
  LMove( 2, c(+2,0,0), c(0,0,+2)) %>%  
  LMove( 8, c(+2,0,0), c(0,0,-2)) %>%  
  LMove(14, c(-2,0,0), c(0,0,+2));
## Bots now in position at the corners, and have filled and voided one level
for(level in 1:11){ ## Only fill up to third to last level to leave a hole for bots
  d <- 27 - (level*2);
  v <- d - 4;
  systemState %<>%
    GFill(1, c(1,0,1), c(d,0,d)) %>%
    GFill(2, c(-1,0,-1), c(-d,0,-d)) %>%
    GFill(8, c(-1,0,1), c(-d,0,d)) %>%
    GFill(14, c(1,0,-1), c(d,0,-d)) %>%
    LMove(1, c(3,0,0), c(0,1,0)) %>%  
    LMove(2, c(-3,0,0), c(0,1,0)) %>%  
    LMove(8, c(-3,0,0), c(0,1,0)) %>%  
    LMove(14, c(3,0,0), c(0,1,0)) %>%  
    SMove(1, c(0,0,3)) %>%  
    SMove(2, c(0,0,-3)) %>%  
    SMove(8, c(0,0,3)) %>%  
    SMove(14, c(0,0,-3)) %>%  
    GVoid(1, c(0,-1,0), c(v,0,v)) %>%
    GVoid(2, c(0,-1,0), c(-v,0,-v)) %>%
    GVoid(8, c(0,-1,0), c(-v,0,v)) %>%
    GVoid(14, c(0,-1,0), c(v,0,-v)) %>%
    LMove( 1, c(-2,0,0), c(0,0,-2)) %>%  
    LMove( 2, c(+2,0,0), c(0,0,+2)) %>%  
    LMove( 8, c(+2,0,0), c(0,0,-2)) %>%  
    LMove(14, c(-2,0,0), c(0,0,+2));
  ##ShowField();
}
## All voids done, now fill the last two layers
systemState %<>%
  GFill(1, c(1,0,1), c(3,0,3)) %>%
  GFill(2, c(-1,0,-1), c(-3,0,-3)) %>%
  GFill(8, c(-1,0,1), c(-3,0,3)) %>%
  GFill(14, c(1,0,-1), c(3,0,-3)) %>%
  LMove(1, c(1,0,0), c(0,1,0)) %>%  
  LMove(2, c(-1,0,0), c(0,1,0)) %>%  
  LMove(8, c(-1,0,0), c(0,1,0)) %>%  
  LMove(14, c(1,0,0), c(0,1,0)) %>%  
  SMove(1, c(0,0,1)) %>%  
  SMove(2, c(0,0,-1)) %>%  
  SMove(8, c(0,0,1)) %>%  
  SMove(14, c(0,0,-1)) %>%
  Fill(1, c(1,0,1)) %>%
  Fill(2, c(-1,0,-1)) %>%
  Fill(8, c(-1,0,1)) %>%
  Fill(14, c(1,0,-1));
## Pyramid is done, now just need to collect the bots
systemState %<>%
  Wait(1) %>%
  SMove(2, c(-2,0,0)) %>%
  SMove(8, c(-2,0,0)) %>%
  Wait(14) %>%
  FusionP( 1, c( 1,0,0),  8, c(-1,0,0)) %>%
  FusionS( 2, c(-1,0,0), 14, c( 1,0,0)) %>%
  FusionS( 8, c(-1,0,0),  1, c( 1,0,0)) %>%
  FusionP(14, c( 1,0,0),  2, c(-1,0,0)) %>%
  Wait(1) %>%
  SMove(14, c(0,0,-2)) %>%
  FusionP(1, c(0,0,1), 14, c(0,0,-1)) %>%
  FusionS(14, c(0,0,-1), 1, c(0,0,1)) %>%
  SMove(1, c(-13,0,0)) %>%
  SMove(1, c(0,0,-13)) %>%
  SMove(1, c(0,-13,0)) %>%
  Halt(1); ## Note: Halt closes the trace file even if the command fails
