source("botFunctions.r");

## Fill a cube (FA019)

systemState <- loadBoard(systemState, "../problems/FA019_tgt.mdl") %>%
  Fission(1, c(0,1,0), 15) %>%
  Fission(1, c(1,0,0), 10) %>%
  SMove(2, c(0,15,0)) %>%
  Fission(1, c(0,0,1), 7) %>%
  SMove(2, c(0,12,0)) %>%
  SMove(18, c(15,0,0)) %>%
  Wait(1) %>%
  Fission(2, c(0,0,1), 10) %>%
  SMove(18, c(13,0,0)) %>%
  SMove(29, c(0,0,15)) %>%
  Wait(1) %>%
  Fission(2, c(1,0,0), 2) %>%
  SMove(3, c(0,0,15)) %>%
  Fission(18, c(0,0,1), 5) %>%
  SMove(29, c(0,0,13)) %>%
  Wait(1) %>%
  Wait(2) %>%
  SMove(3, c(0,0,13)) %>%
  SMove(14, c(15,0,0)) %>%
  Wait(18) %>%
  SMove(19, c(0,0,15)) %>%
  Fission(29, c(0,1,0), 5) %>%
  Wait(1) %>%
  Wait(2) %>%
  SMove(3, c(15,0,0)) %>%
  SMove(14, c(13,0,0)) %>%
  Wait(18) %>%
  SMove(19, c(0,0,13)) %>%
  Wait(29) %>%
  SMove(30, c(0,15,0)) %>%
  Wait(1) %>%
  SMove(2, c(0,0,1)) %>%
  SMove(3, c(13,0,0)) %>%
  SMove(14, c(0,0,1)) %>%
  Wait(18) %>%
  Wait(19) %>%
  Wait(29) %>%
  SMove(30, c(0,11,0)) %>%
  ## 28x28x28 cube
  GFill(1,  c( 1, 0, 1), c( 27, 27, 27)) %>%
  GFill(2,  c( 1,-1, 0), c( 27,-27, 27)) %>%
  GFill(3,  c( 0,-1,-1), c(-27,-27,-27)) %>%
  GFill(14, c(-1,-1, 0), c(-27,-27, 27)) %>%
  GFill(18, c(-1, 0, 1), c(-27, 27, 27)) %>%
  GFill(19, c(-1, 0,-1), c(-27, 27,-27)) %>%
  GFill(29, c( 1, 0,-1), c( 27, 27,-27)) %>%
  GFill(30, c( 1, 0,-1), c( 27,-27,-27)) %>%
  Wait(1) %>%
  Wait(2) %>%
  SMove(3, c(-15,0,0)) %>%
  SMove(14, c(-15,0,0)) %>%
  SMove(18, c(-15,0,0)) %>%
  SMove(19, c(-15,0,0)) %>%
  Wait(29) %>%
  Wait(30) %>%
  Wait(1) %>%
  Wait(2) %>%
  SMove(3, c(-13,0,0)) %>%
  SMove(14, c(-13,0,0)) %>%
  SMove(18, c(-13,0,0)) %>%
  SMove(19, c(-13,0,0)) %>%
  Wait(29) %>%
  Wait(30) %>%
  FusionP(1, c(1,0,0), 18, c(-1,0,0)) %>%
  FusionP(2, c(1,0,0), 14, c(-1,0,0)) %>%
  FusionS(3, c(0,-1,0), 30, c(0,1,0)) %>%
  FusionS(14, c(-1,0,0), 2, c(1,0,0)) %>%
  FusionS(18, c(-1,0,0), 1, c(1,0,0)) %>%
  FusionS(19, c(-1,0,0), 29, c(1,0,0)) %>%
  FusionP(29, c(1,0,0), 19, c(-1,0,0)) %>%
  FusionP(30, c(0,1,0), 3, c(0,-1,0)) %>%
  Wait(1) %>%
  SMove(2, c(0,-15,0)) %>%
  Wait(29) %>%
  SMove(30, c(0,-15,0)) %>%
  Wait(1) %>%
  SMove(2, c(0,-13,0)) %>%
  Wait(29) %>%
  SMove(30, c(0,-11,0)) %>%
  FusionP(1, c(0,0,1), 2, c(0,0,-1)) %>%
  FusionS(2, c(0,0,-1), 1, c(0,0,1)) %>%
  FusionP(29, c(0,1,0), 30, c(0,-1,0)) %>%
  FusionS(30, c(0,-1,0), 29, c(0,1,0)) %>%
  Wait(1) %>%
  SMove(29, c(0,0,-15)) %>%
  Wait(1) %>%
  SMove(29, c(0,0,-13)) %>%
  FusionP(1, c(0,0,1), 29, c(0,0,-1)) %>%
  FusionS(29, c(0,0,-1), 1, c(0,0,1)) %>%
  Halt(1);

