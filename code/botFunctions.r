library(bit);
library(bitops);
library(dplyr);

systemState <- list();

loadBoard <- function(state, modelFile){
  ## reset state to ground level
  state$energy <- 0;
  state$harmonics <- factor("Low", levels = c("Low","High"));
  state$res <- 1;
  state$matrix <- array(bit(1), dim = c(1,1,1));
  state$target <- array(bit(1), dim = c(1,1,1));
  state$bots <- list(list(bid=1, pos=c(0,0,0), seeds=2:38));
  state$trace <- list();
  state$traceBuffer <- list();
  
  fileLen <- file.size(modelFile);
  fileData <- readBin(modelFile, what=integer(), size=1, signed=FALSE,
                      n=fileLen);
  state$modelName <- basename(modelFile);
  chunkMat <- 2^(0:7);
  modelRes <- fileData[1];
  fileData <- fileData[-1];
  matrixData <- as.bit(bitAnd(rep(fileData, each=8), chunkMat) > 0);
  matrixData <- head(matrixData, modelRes^3);
  dim(matrixData) <- rep(modelRes, 3);
  state$res <- modelRes;
  state$target <- matrixData;
  state$matrix <- matrixData;
  state$matrix[state$matrix] <- FALSE;
  state$traceBuffer = file("trace.nbt", open="wb");
  return(state);
}

shl <- bitShiftL;

## Non-standard function: displays the current field on the Y-plane of bot 0
ShowField <- function(state){
  YLevel <- state$bots[[1]]$pos[2];
  par(mar=c(2,2,2,2));
  plot(NA, xlim=c(0,state$res-1), ylim=c(0,state$res-1), axes=FALSE, ann=FALSE);
  mtext(text = sprintf("Model: %s; Y Level: %d", state$modelName, YLevel),
        cex=1.5);
  axis(1);
  axis(2);
  segments(x0 = -0.5:(state$res-0.5), y0 = -0.5, y1 = state$res-0.5);
  segments(x0 = -0.5, x1 = state$res-0.5, y0 = -0.5:(state$res-0.5));
  for(y in 0:(state$res-1)){
      for(x in 0:(state$res-1)){
        posCode <- (state$target[x+1,YLevel+1,y+1]*2) + (state$matrix[x+1,YLevel+1,y+1]+0);
        if(posCode > 0){
          rect(xleft = x - 0.3, xright = x + 0.3,
               ybottom = y - 0.3, ytop = y + 0.3,
               col = c("cornflowerblue","cornsilk","darkgreen")[posCode]);
        }
      }
  }
  for(b in state$bots){
    rect(xleft = b$pos[1] - 0.2, xright = b$pos[1] + 0.2,
         ybottom = b$pos[3] - 0.2, ytop = b$pos[3] + 0.2,
         col = "gold");
    text(b$bid, x=b$pos[1], y=b$pos[3], cex=0.5);
  }
  return(invisible(state));
}

Halt <- function(state, bid){
  ## Note: Halt *always* succeeds, and closes the trace buffer
  success <- TRUE;
  if(!is.null(state$traceBuffer)){
    writeBin(con=state$traceBuffer, as.raw(strtoi("11111111", 2)), useBytes=TRUE );
    close(state$traceBuffer);
    state$traceBuffer <- NULL;
  }
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(length(state$bots) > 1){
    success <- FALSE;
    cat("Error: conditions not suitable to halt system; more than 1 bot\n");
  }
  if(any(state$bots[[1]]$pos != c(0,0,0))){
    success <- FALSE;
    cat(sprintf(paste0("Error: conditions not suitable to halt system; ",
                        "bot is at (%s), not at origin (0,0,0)\n"), 
                 paste(state$bots[[1]]$pos, collapse=",")));
  }
  if(state$harmonics != "Low"){
    success <- FALSE;
    cat("Error: conditions not suitable to halt system; harmonics are not low\n");
  }
  if(!all(state$matrix == state$target)){
    success <- FALSE;
    cat("Error: problem matrix does not match target\n");
  }
  if(success){
    state$bots <- list();
    cat(sprintf("Finished successfully! Final energy %s\n", state$energy));
  } else {
    cat(sprintf("Failed! Final energy %s\n", state$energy));
    ShowField(state);
  }
  return(state);
}

Wait <- function(state, bid){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  writeBin(con=state$traceBuffer, as.raw(strtoi("11111110", 2)), useBytes=TRUE);
  return(state);
}

Flip <- function(state, bid){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  state$harmonics <- ifelse(state$harmonics == "Low", "High", "Low");
  writeBin(con=state$traceBuffer, as.raw(strtoi("11111101", 2)), useBytes=TRUE);
  return(state);
}

SMove <- function(state, bid, lld){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(sum(lld != 0) != 1){
    stop("Error: lld must be a movement in only one direction");
  }
  c1 <- state$bots[[bpos]]$pos + lld;
  if(any(c1 < 0) || any(c1 >= state$res)){
    stop("Error: movement result must be within the bounds of the build area");
  }
  ## TODO: check to make sure nothing in the path is full
  state$bots[[bpos]]$pos <- c1;
  state$energy <- state$energy + 2 * sum(abs(lld));
  ## Note: bot order is deterministic / sequential, so botID is not emitted in the trace
  writeBin(con=state$traceBuffer, 
           as.raw(c(shl(which(lld != 0),4) + strtoi("0100", 2),
                  sum(lld) + 15)), 
           useBytes=TRUE);
  return(state);
}

LMove <- function(state, bid, sld1, sld2){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(sum(sld1 != 0) != 1){
    stop("Error: sld1 must be a movement in only one direction");
  }
  if(sum(sld2 != 0) != 1){
    stop("Error: sld2 must be a movement in only one direction");
  }
  if(sum(abs(sld1)) > 5){
    stop("Error: sld2 must be a movement of no more than 5 units in one direction");
  }
  if(sum(abs(sld2)) > 5){
    stop("Error: sld2 must be a movement of no more than 5 units in one direction");
  }
  c1 <- (state$bots[[bpos]])$pos + sld1;
  c2 <- c1 + sld2;
  if(any(c1 < 0) || any(c1 >= state$res)){
    stop("Error: intermediate movement must be within the bounds of the build area");
  }
  if(any(c2 < 0) || any(c2 >= state$res)){
    stop("Error: movement result must be within the bounds of the build area");
  }
  ## TODO: check to make sure nothing in the path is full
  state$bots[[bpos]]$pos <- c2;
  state$energy <- state$energy + 2 * sum(abs(sld1), 2, abs(sld2));
  writeBin(con=state$traceBuffer, 
           as.raw(
           c(shl(which(sld2 != 0),6) + shl(which(sld1 != 0),4) + strtoi("1100", 2),
             shl(sld2[sld2 != 0] + 5, 4) + (sld1[sld1 != 0] + 5))), 
           useBytes=TRUE);
  return(state);
}

Fill <- function(state, bid, nd){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(!(sum(abs(nd)) %in% c(1,2))){
    stop("Error: nd must be a location no more than 2 manhattan units away");
  }
  if(max(abs(nd)) > 1){
    stop("Error: nd must be a location no more than 1 chessboard unit away");
  }
  c1 <- state$bots[[bpos]]$pos + nd;
  if(!state$matrix[c1[1]+1,c1[2]+1,c1[3]+1]){
    state$matrix[c1[1]+1,c1[2]+1,c1[3]+1] <- TRUE;
    state$energy <- state$energy + 6;
  }
  state$energy <- state$energy + 6;
  ## (dx + 1) * 9 + (dy + 1) * 3 + (dz + 1)
  writeBin(con=state$traceBuffer, 
           as.raw(shl(sum((nd+1) * 3^(2:0)), 3) + strtoi("011", 2)), 
           useBytes=TRUE);
  return(state);
}

Void <- function(state, bid, nd){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(!(sum(abs(nd)) %in% c(1,2))){
    stop("Error: nd must be a location no more than 2 manhattan units away");
  }
  if(max(abs(nd)) > 1){
    stop("Error: nd must be a location no more than 1 chessboard unit away");
  }
  c1 <- state$bots[[bpos]]$pos + nd;
  if(!state$matrix[c1[1]+1,c1[2]+1,c1[3]+1]){
    state$matrix[c1[1]+1,c1[2]+1,c1[3]+1] <- FALSE;
    state$energy <- state$energy - 15;
  }
  state$energy <- state$energy + 3;
  ## (dx + 1) * 9 + (dy + 1) * 3 + (dz + 1)
  writeBin(con=state$traceBuffer, 
           as.raw(shl(sum((nd+1) * 3^(2:0)), 3) + strtoi("010", 2)), 
           useBytes=TRUE);
  return(state);
}

Fission <- function(state, bid, nd, m){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(!(sum(abs(nd)) %in% c(1,2))){
    stop("Error: nd must be a location no more than 2 manhattan units away");
  }
  if(max(abs(nd)) > 1){
    stop("Error: nd must be a location no more than 1 chessboard unit away");
  }
  if(length(state$bots[[bpos]]$seeds) < (m+1)){
    stop(sprintf("Error: not enough bot seeds in bot %d to split at %d (it only has %d)", 
                 bid, m, length(state$bots[[bpos]]$seeds)));
  }
  
  c1 <- state$bots[[bpos]]$pos + nd;

  if(any(c1 < 0) || any(c1 >= state$res)){
    stop("Error: fissed bot must be within the bounds of the build area");
  }
  
  bot1 <- list(bid=state$bots[[bpos]]$seeds[1], pos=c1, seeds=state$bots[[bpos]]$seeds[2:(m+1)]);
  
  state$bots[[bpos]]$seeds <- state$bots[[bpos]]$seeds[-(1:(m+1))];
  state$bots <- append(state$bots, list(bot1));
  state$bots <- state$bots[order(sapply(state$bots, function(x){x$bid}))];
  state$energy <- state$energy + 24;
  
  writeBin(con=state$traceBuffer, 
           as.raw(c(shl(sum((nd+1) * 3^(2:0)), 3) + strtoi("101", 2),
                    m)), 
           useBytes=TRUE);
  return(state);
}

## The problem with fusion is that it *should* be an atomic operation,
## but FusionS may appear before FusionP, and there may be intermediate bot operations
## This is worked around by carrying out all the operations the first time either is called, 
## and only doing the file writing the second time either is called (when bidS cannot be found)
Fusion <- function(state, bidP, ndP, bidS, ndS){
  if(bidS %in% sapply(state$bots, function(x){x$bid})){
    bposP <- which(sapply(state$bots, function(x){x$bid}) == bidP);
    if(!(sum(abs(ndP)) %in% c(1,2))){
      stop("Error: ndP must be a location no more than 2 manhattan units away");
    }
    if(max(abs(ndP)) > 1){
      stop("Error: ndP must be a location no more than 1 chessboard unit away");
    }
    bposS <- which(sapply(state$bots, function(x){x$bid}) == bidS);
    if(!(sum(abs(ndS)) %in% c(1,2))){
      stop("Error: ndS must be a location no more than 2 manhattan units away");
    }
    if(max(abs(ndS)) > 1){
      stop("Error: ndS must be a location no more than 1 chessboard unit away");
    }
    c1 <- state$bots[[bposP]]$pos + ndP;
    c2 <- state$bots[[bposS]]$pos + ndS;
    if((state$bots[[bposP]]$pos != c2) || (state$bots[[bposS]]$pos != c1)){
      stop(sprintf("Error: Fusion points do not match: (%s) vs (%s); (%s) vs (%s)",
                   paste(state$bots[[bposP]]$pos, collapse=","), paste(c2, collapse=","),
                   paste(state$bots[[bposS]]$pos, collapse=","), paste(c1, collapse=",")));
    }
    state$bots[[bposP]]$seeds <- 
      sort(c(state$bots[[bposP]]$seeds,state$bots[[bposS]]$bid,state$bots[[bposS]]$seeds));
    newBots <- list();
    state$bots <- state$bots[-bposS];
    state$energy <- state$energy - 24;
  }
  return(state);
}

FusionP <- function(state, bidP, ndP, bidS, ndS){
  state <- Fusion(state, bidP, ndP, bidS, ndS);
  writeBin(con=state$traceBuffer,
           as.raw(shl(sum((ndP+1) * 3^(2:0)), 3) + strtoi("111", 2)), 
           useBytes=TRUE);
  return(state);
}

FusionS <- function(state, bidS, ndS, bidP, ndP){
  state <- Fusion(state, bidP, ndP, bidS, ndS);
  writeBin(con=state$traceBuffer, 
           as.raw(shl(sum((ndS+1) * 3^(2:0)), 3) + strtoi("110", 2)), 
           useBytes=TRUE);
  return(state);
}

GFill <- function(state, bid, nd, fd){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(!(sum(abs(nd)) %in% c(1,2))){
    stop("Error: nd must be a location no more than 2 manhattan units away");
  }
  if(max(abs(nd)) > 1){
    stop("Error: nd must be a location no more than 1 chessboard unit away");
  }
  c1 <- state$bots[[bpos]]$pos + nd;
  c2 <- c1 + fd;
  ## only fill matrix if all directions are positive; this makes sure it is
  ## only done once
  if(!any(fd < 0)){
    for(x in c1[1]:c2[1]){
      for(y in c1[2]:c2[2]){
        for(z in c1[3]:c2[3]){
          if(!state$matrix[x+1,y+1,z+1]){
            state$energy <- state$energy + 6;
          }
          state$matrix[x+1,y+1,z+1] <- TRUE;
          state$energy <- state$energy + 6;
        }
      }
    }
  }
  writeBin(con=state$traceBuffer,
           as.raw(c(shl(sum((nd+1) * 3^(2:0)), 3) + strtoi("001", 2),
                    fd[1]+30,
                    fd[2]+30,
                    fd[3]+30)), 
           useBytes=TRUE);
  return(state);
};

GVoid <- function(state, bid, nd, fd){
  bpos <- which(sapply(state$bots, function(x){x$bid}) == bid);
  if(!(sum(abs(nd)) %in% c(1,2))){
    stop("Error: nd must be a location no more than 2 manhattan units away");
  }
  if(max(abs(nd)) > 1){
    stop("Error: nd must be a location no more than 1 chessboard unit away");
  }
  c1 <- state$bots[[bpos]]$pos + nd;
  c2 <- c1 + fd;
  ## only void matrix if all directions are positive; this makes sure it is
  ## only done once
  if(!any(fd < 0)){
    for(x in c1[1]:c2[1]){
      for(y in c1[2]:c2[2]){
        for(z in c1[3]:c2[3]){
          if(!state$matrix[x+1,y+1,z+1]){
            state$energy <- state$energy + 15;
          }
          state$matrix[x+1,y+1,z+1] <- FALSE;
          state$energy <- state$energy - 12;
        }
      }
    }
  }
  writeBin(con=state$traceBuffer,
           as.raw(c(shl(sum((nd+1) * 3^(2:0)), 3) + strtoi("000", 2),
                    fd[1]+30,
                    fd[2]+30,
                    fd[3]+30)), 
           useBytes=TRUE);
  return(state);
};
